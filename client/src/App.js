import "./App.css";

function App() {
  return (
    <div className="App">
      <div className="card borderform">
        <h1 className="mymargin mt-4 myfonts">Tambah Peserta</h1>
        <form className="mb-4">
          <div className="form-group myfonts">
            <label for="exampleInputEmail1">Username</label>
            <input
              type="text"
              className="form-control mx-auto border-success"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
            />
          </div>
          <div className="form-group myfonts">
            <label for="exampleInputEmail1">Email address</label>
            <input
              type="email"
              className="form-control mx-auto border-success"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
            />
          </div>
          <div className="form-group myfonts">
            <label for="exampleInputPassword1">Password</label>
            <input
              type="password"
              className="form-control mx-auto border-success"
              id="exampleInputPassword1"
            />
          </div>
          <button type="submit" className="btn btn-primary">
            Submit
          </button>
        </form>
      </div>
    </div>
  );
}

export default App;
