import React, { Component } from "react";

class Cari extends Component {
  render() {
    return (
      <nav class="navbar navbar-light bg-light">
        <form class="form-inline">
          <input
            class="form-control mr-sm-2"
            type="search"
            placeholder="Search"
            aria-label="Search"
          />
          <button
            class="btn btn-outline-success my-2 my-sm-0"
            type="submit"
            href="/Home"
          >
            Search
          </button>
        </form>
      </nav>
    );
  }
}

export default Cari;
