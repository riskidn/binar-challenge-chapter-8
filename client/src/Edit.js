import React, { Component } from "react";

class Edit extends Component {
  render() {
    return (
      <div className="App">
        <div className="card borderform">
          <h1 className="mymargin mt-4 myfonts">Edit Peserta</h1>
          <form className="mb-4">
            <div className="form-group myfonts">
              <label for="exampleInputEmail1">Username</label>
              <input
                type="text"
                className="form-control mx-auto border-success"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
              />
            </div>
            <div className="form-group myfonts">
              <label for="exampleInputEmail1">Email address</label>
              <input
                type="email"
                className="form-control mx-auto border-success"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
              />
            </div>
            <div className="form-group myfonts">
              <label for="exampleInputPassword1">New Password</label>
              <input
                type="password"
                className="form-control mx-auto border-success"
                id="exampleInputPassword1"
              />
            </div>
            <div className="form-group myfonts">
              <label for="exampleInputPassword1">Konfirmasi Password</label>
              <input
                type="password"
                className="form-control mx-auto border-success"
                id="exampleInputPassword1"
              />
            </div>
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default Edit;
