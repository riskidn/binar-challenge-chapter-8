import "./App.css";
import React, { Component } from "react";
import { Route, NavLink, HashRouter } from "react-router-dom";
import Home from "./App";
import Edit from "./Edit";
import Cari from "./Cari";

class Navbar extends Component {
  render() {
    return (
      <HashRouter>
        <ul className="MenuAtas">
          <li>
            <NavLink to="/">Beranda</NavLink>
          </li>
          <li>
            <NavLink to="/edit">Edit</NavLink>
          </li>
          <li>
            <NavLink to="/cari">Pencarian</NavLink>
          </li>
        </ul>
        <div className="content">
          <Route exact path="/" component={Home} />
          <Route path="/Edit" component={Edit} />
          <Route path="/Cari" component={Cari} />
        </div>
      </HashRouter>
    );
  }
}
export default Navbar;
